@extends('appbase')

@section('head')
    <title>Filmpjes</title>
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title">
            </div>
            <div id="videos" class="">
                @for($i = 0; $i < count($videoIds);$i++)
                    <div class="videoContainer border">
                        <iframe class="iframe" width="400" height="200" src="https://www.youtube.com/embed/{{$videoIds[$i]}}"></iframe>
                        <h3>{{$titles[$i]}}</h3>
                        <p class="parDis">{{$description[$i]}}</p>
                    </div>
                @endfor
                </div>
            </div>
        </div>
    </div>
@endsection
