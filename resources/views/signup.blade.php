@extends('appbase')
@section('head')
    <title>Registreren</title>
@endsection
@section('content')
<div class="container">
    <img class="loginLogo" src="{{asset('imgs/mainLogo.png')}}">
    <div class="modal-content">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <div class="formInput">
                    <div class="input-group mb-3 flex-center">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <input type="text" name="name" class="inputText flex-center" placeholder="naam" aria-label="naam">
                    </div>
                    <div class="input-group mb-3 flex-center">
                            <span class="input-group-text">
                                    <i class="fa fa-envelope"></i>
                                </span>
                        <input type="email" name="email" class="inputText" placeholder="E-mail" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 flex-center">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-key"></i>
                                </span>
                        </div>
                        <input type="password" name="password" class="inputText flex-center" placeholder="Password" aria-label="Password">
                    </div>
                    <div class="input-group mb-3 flex-center">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                        </div>
                        <input type="date" name="dob" class="inputText flex-center" placeholder="geboortedatum" aria-label="geboortedatum">
                    </div>
                    <div class="flex-center form-check">
                        <input type="checkbox" value="1" name="algm" class="checkInput" aria-label="algemene voorwaarden">
                        <label class="form-check-label">algemene voorwaarden</label>
                    </div>
                    <div class="input-group-append flex-center loginButton">
                        <button class="btn btn-lg btn-outline-primary" type="submit">{{ __('Registreren') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer')@endsection
