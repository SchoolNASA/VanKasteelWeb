@extends('appbase')
@section('head')
    <title>Inlogen</title>
@endsection
@section('content')
    <div class="container">
        <img class="loginLogo" src="{{asset('imgs/mainLogo.png')}}">
        <div class="modal-content">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <div class="formInput">
                        <div class="input-group mb-3 flex-center">
                            <span class="input-group-text">
                                    <i class="fa fa-envelope"></i>
                                </span>
                            <input type="email" name="email" class="inputText" placeholder="E-mail" aria-label="Email" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3 flex-center">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-key"></i>
                                </span>
                            </div>
                            <input type="password" name="password" class="inputText flex-center" placeholder="Password" aria-label="Password">
                        </div>
                        <div class="input-group small flex-center">
                            <a class="btn-link" href="#"><h6>Wachtwoord vergeten?</h6></a>
                        </div>
                        <div class="input-group-append flex-center loginButton">
                            <button  class="btn btn-lg btn-outline-primary" type="submit">{{ __('Login') }}</button>
                        </div>
                        <div class="infoForm text-info flex-center">
                        <h4>Heeft U nog geen account?</h4>
                        </div>
                        <div class="input-group-append flex-center regButton">
                            <a class="btn btn-lg" href="{{url('signup')}}">Account aanmaken</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
