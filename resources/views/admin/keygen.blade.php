@extends('appbase')
@section('head')
    <title>Key Generator</title>
    @yield('scripts')
@endsection
@section('content')
    @if(session('Status'))
        <p class="flex-center">{{session('Status')}}</p>
    @endif

    <header class="flex-center">
        <img id="adminLogo" class="card-img-top" src="{{asset('imgs/vkgrdicht.png')}}">
    </header>

    <form type="POST" action="{{route('keygen')}}">
        <div class="align-content-center main-keygen">
            <div class="flex-center codeGenerator">
                <input type="text" value="{{$key}}">
            </div>
            <div class="flex-center">
                <button class="btn btn-info" type="submit" name = "submit" value = "Update">Genereer een key</button>
            </div>
        </div>
    </form>
@endsection