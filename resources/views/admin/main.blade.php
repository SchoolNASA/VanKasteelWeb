@extends('appbase')
@section('head')
    <title>Admin Controller</title>
    @yield('scripts')
@endsection
@section('content')
    @if(session('Status'))
        <p class="flex-center">{{session('Status')}}</p>
    @endif
    <header class="flex-center">
        <img id="adminLogo" class="card-img-top" src="{{asset('imgs/vkgrdicht.png')}}">
    </header>

    <div class="flex-center mainControl">
        <a href="{{route('keygen')}}">
            <i class="fa fa-5x fa-address-book"></i>
            <p>Gebruikers</p>
        </a>
    </div>
@endsection