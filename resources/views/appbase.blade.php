<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @yield('head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<!----------Fonts--------->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!----------Style--------->
<link media="all" type="text/css" rel="stylesheet" href="{{asset('css/main.css')}}">
<link media="all" type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}">

<!----------Scripts------->
    @yield('scripts')
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="/js/app.js"></script>
        <script src="{{asset('js/general.js')}}"></script>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
@yield('footer')
<footer class="footer fixed-bottom text-center m-md-2">&copy; Van kasteel 2018</footer>

</html>