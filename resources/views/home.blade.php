@extends('appbase')
@section('head')
        <title>Home</title>
        @yield('scripts')
@endsection
    @section('content')
        @if(session('Status'))
            <p class="flex-center">{{session('Status')}}</p>
        @endif
        <div class="flex-center">
            <div class="content">
                <div class="pageTitle">
                    <img id="homeLogo" src="{{asset('imgs/logoWithText.png')}}">
                </div>
                <div class="container">
                    <div class="sitelinks">
                        <div class="pull-left leftLinks">
                            <a class="btn btn-light" href="#">
                                <i class="fa fa-envelope fa-5x"></i>
                                <p>Berichten</p>
                            </a>
                            <div class="bottomleftLink">
                                <a class="btn btn-light" href="#">
                                    <i class="fa fa-newspaper-o fa-5x"></i>
                                    <p>Nieuws</p>
                                </a>
                            </div>
                        </div>
                    {{--</br>--}}
                        <div class="pull-right rightLinks">
                            <a class="btn btn-light" href="{{url('videos')}}">
                                <i class="fa fa-youtube-play fa-5x"></i>
                                <p>Filmpjes</p>
                            </a>

                            <div class="bottomLinks bottomRightLink">
                            <a class="btn btn-light" href="#">
                                <i class="fa fa-check-square-o fa-5x"></i>
                                <p>Vragenlijst</p>
                            </a>
                            </div>
                        </div>
                        <div class="logoutLink">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-sign-out fa-5x"></i>
                                    <p>Uitloggen</p>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('footer')@endsection
