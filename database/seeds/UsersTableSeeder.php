<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(

            ['name' => 'admin',
            'email' => "admin@nasa.nl",
            'password' => bcrypt('admin'),
            'algm' => true,
    ]);
    }
}
