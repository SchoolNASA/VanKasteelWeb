<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@dashboard')->name('dashboard');
Route::get('/home', 'HomeController@dashboard')->name('dashboard');

Route::get('/login','AuthController@showloginForm')->name('login');
Route::post('/login', 'AuthController@login');

Route::get('/signup', 'AuthController@showRegisterForm')->name('register');
Route::post('/signup', 'AuthController@RegisterForm');

Route::post('/logout', 'AuthController@logout')->name('logout');;

Route::get('/videos', 'VideosController@ViewAction')->name('videos');

Route::get('/admin', 'AdminController@ViewAction')->name('admin');
Route::get('/admin/keygen', 'AdminController@KeyGenerator')->name('keygen');
Route::post('/admin/keygen', 'AdminController@KeyGenerator');
