<?php

namespace App\Http\Controllers;

use App\User;
use http\Exception\RuntimeException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showRegisterForm()
    {
        return view('signup');
    }

    public function RegisterForm(Request $request)
    {
        $this->validation($request);
        $request['password'] = bcrypt($request->password);
        User::create($request->all());

        return redirect('/')->with('Status', 'U bent geregistrerd');
    }

    public function validation(Request $request)
    {
        return $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'algm' => 'required'
        ]);
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email|max:255',
            'password' => 'required|max:255'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)){
//            if ($request->email === 'admin@nasa.nl'){
//                return redirect('/admin');
//            } else {
                return redirect('/home');
//            }
        }

        //Error messages
        $messages = [
            "email.required" => "Email is required",
            "email.email" => "Email is not valid",
            "email.exists" => "Email doesn't exists",
            "password.required" => "Password is required",
            "password.mismatch" => "Password is incorrect"
        ];

        // validate the form data
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ], $messages);

        return back()->withErrors("password is incorrect");
    }

    public function showloginForm()
    {
        return view('login');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }

}
