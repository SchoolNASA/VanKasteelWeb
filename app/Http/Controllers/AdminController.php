<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends AuthController
{

    public function ViewAction()
    {
        return view('admin.main');
    }

    /**
     * Generate a "random" alpha-numeric string.
     *
     * Should not be considered sufficient for cryptography, etc.
     *
     * @param  int  $length
     * @return string
     */
    public static function KeyGenerator($length = 16, Request $request)
    {
        $key = "";

        if($request->submit == "Update")
        {
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
        }

        return view('admin.keygen')->with
            ([
                'key' => $key
            ]);

    }
}