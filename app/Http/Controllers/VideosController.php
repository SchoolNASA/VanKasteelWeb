<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use Symfony\Component\HttpFoundation\Request;

class VideosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ViewAction(Request $request)
    {
        if($this->getMiddleware()){
            $videosList = Youtube::listChannelVideos('UCf71UG7b7xAC12AfUqBUaug');
            $titles = [];
            $videoIds =[];
            $videoDis = [];

                for ($i = 0; $i < count($videosList); $i++){
                    $titles[] = $videosList[$i]->snippet->title;
                    $videoIds[] = $videosList[$i]->id->videoId;
                    $videoDis[] = $videosList[$i]->snippet->description;
                }

            return view('videos')->with
            ([
                'titles' => $titles,
                'videoIds' => $videoIds,
                'description' => $videoDis
            ]);
        }

        return redirect('/login');
    }
}