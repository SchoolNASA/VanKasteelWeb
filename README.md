# Project van Kasteel


## To run the project successfully, follow these steps:

### 1. import the project's code to your webserver directory 
### 2. run this command in your terminal 

~~~~
composer install
~~~~

### 3. run this command in your terminal to migrate entities into the database

~~~~
php artisan migrate
~~~~

### 4. to run the the project's server (development mode)

~~~~
php artisan serve
~~~~

### (optional) 
### to run the project smartphone friendly, use this command

~~~~
php artisan serve --host 0.0.0.0
~~~~

### and browse to the host's ip address

## ps:

#### please make sure that you have composer installed 


##### don't hesitate to email me when needed:

~~~~
a.khamees@student.alfa-college.nl
~~~~